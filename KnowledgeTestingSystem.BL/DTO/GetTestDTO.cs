﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.DTO
{
    public class GetTestDTO
    {
        public GetTestDTO()
        {
            Questions = new List<QuestionDTO>();
        }
        public int TestDurationInMinutes { get; set; }
        public string TestName { get; set; }
        public List<QuestionDTO> Questions { get; set; }
    }

    public class QuestionDTO
    {
        public QuestionDTO()
        {
            AnswersDto = new List<AnswerDTO>();
        }
        public string QuestionText { get; set; }
        public List<AnswerDTO> AnswersDto { get; set; }
    }

    public class AnswerDTO
    {
        public string AnswerTextDto { get; set; }
    }
}
