namespace KnowledgeTestingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Tests", name: "TestOwner_Id", newName: "TestOwnerId");
            RenameIndex(table: "dbo.Tests", name: "IX_TestOwner_Id", newName: "IX_TestOwnerId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Tests", name: "IX_TestOwnerId", newName: "IX_TestOwner_Id");
            RenameColumn(table: "dbo.Tests", name: "TestOwnerId", newName: "TestOwner_Id");
        }
    }
}
