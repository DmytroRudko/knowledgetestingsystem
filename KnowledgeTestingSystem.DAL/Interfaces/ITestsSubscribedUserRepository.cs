﻿using KnowledgeTestingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Interfaces
{
    public interface ITestsSubscribedUserRepository
    {
        ICollection<Test> GetUserSubscribedTest(UserProfile userProfile);
        ICollection<TestsSubscribedUser> GetTestsSubscribedUser(UserProfile userProfile);
        void Add(Test test, UserProfile userProfile);

        Test GetTest(string testName);
        TestsSubscribedUser GetTestsSubscribedUser(Test test, UserProfile userProfile);
        void Save();

    }
}
