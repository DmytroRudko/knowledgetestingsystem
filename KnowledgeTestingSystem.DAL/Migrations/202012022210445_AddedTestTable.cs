namespace KnowledgeTestingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTestTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TestName = c.String(maxLength: 128),
                        TestDesctiption = c.String(),
                        PercentageToComplete = c.Double(nullable: false),
                        TestDuration = c.Time(nullable: false, precision: 7),
                        IsDeleted = c.Boolean(nullable: false),
                        TestOwner_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.TestOwner_Id)
                .Index(t => t.TestOwner_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tests", "TestOwner_Id", "dbo.UserProfiles");
            DropIndex("dbo.Tests", new[] { "TestOwner_Id" });
            DropTable("dbo.Tests");
        }
    }
}
