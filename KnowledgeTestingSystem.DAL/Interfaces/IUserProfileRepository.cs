﻿using KnowledgeTestingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Interfaces
{
    public interface IUserProfileRepository
    {
        UserProfile GetUser(string userName);
    }
}
