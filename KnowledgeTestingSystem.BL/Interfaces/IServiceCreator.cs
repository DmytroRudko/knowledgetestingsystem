﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.Interfaces
{
    //todo: change abstract factory to DI
    public interface IServiceCreator
    {
        IUserService CreateUserService(string connection);
    }
}
