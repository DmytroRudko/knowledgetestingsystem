﻿using KnowledgeTestingSystem.PL.CustomAttribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Models.Test
{
    public class CreateTestModel
    {
        public CreateTestModel()
        {
            TestQuestions = new List<TestQuestionModel>();
        }
        [MaxLength(64, ErrorMessage = "Test name must not exceed 64 characters"),MinLength(3, ErrorMessage ="MinLength = 3")]
        public string TestName { get; set; }

        [MaxLength(256, ErrorMessage = "Test description must not exceed 256 characters")]
        public string TestDesctiption { get; set; }

        [Range(0.0, 100.0, ErrorMessage = "Percent to complete the test must be between 0 and 100")]
        public double PercentageToComplete { get; set; }
        [Range(1, 360,
        ErrorMessage = "Test duration must be between {1} and {2}.")]
        public int TestDuration { get; set; }
        [EnsureOneElement(ErrorMessage = "At least 1 question is required")]
        public List<TestQuestionModel> TestQuestions { get; set; }
    }
}