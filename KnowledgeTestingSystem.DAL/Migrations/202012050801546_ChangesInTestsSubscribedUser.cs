namespace KnowledgeTestingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInTestsSubscribedUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TestsSubscribedUsers", "Test_Id", "dbo.Tests");
            DropIndex("dbo.TestsSubscribedUsers", new[] { "Test_Id" });
            RenameColumn(table: "dbo.TestsSubscribedUsers", name: "Test_Id", newName: "TestId");
            RenameColumn(table: "dbo.TestsSubscribedUsers", name: "User_Id", newName: "UserId");
            RenameIndex(table: "dbo.TestsSubscribedUsers", name: "IX_User_Id", newName: "IX_UserId");
            AlterColumn("dbo.TestsSubscribedUsers", "TestId", c => c.Int(nullable: false));
            CreateIndex("dbo.TestsSubscribedUsers", "TestId");
            AddForeignKey("dbo.TestsSubscribedUsers", "TestId", "dbo.Tests", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestsSubscribedUsers", "TestId", "dbo.Tests");
            DropIndex("dbo.TestsSubscribedUsers", new[] { "TestId" });
            AlterColumn("dbo.TestsSubscribedUsers", "TestId", c => c.Int());
            RenameIndex(table: "dbo.TestsSubscribedUsers", name: "IX_UserId", newName: "IX_User_Id");
            RenameColumn(table: "dbo.TestsSubscribedUsers", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.TestsSubscribedUsers", name: "TestId", newName: "Test_Id");
            CreateIndex("dbo.TestsSubscribedUsers", "Test_Id");
            AddForeignKey("dbo.TestsSubscribedUsers", "Test_Id", "dbo.Tests", "Id");
        }
    }
}
