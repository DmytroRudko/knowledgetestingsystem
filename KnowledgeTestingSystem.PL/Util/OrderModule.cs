﻿using KnowledgeTestingSystem.BL.Interfaces;
using KnowledgeTestingSystem.BL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Util
{
    public class OrderModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserProfileService>().To<UserProfileService>();
            Bind<ITestService>().To<TestService>();
        }
    }
}