﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.BL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.Interfaces
{
    public interface ITestService
    {
        OperationDetails CreateTest(CreateTestDTO createTestDTO);
        OperationDetails SubscribeToTest(string testName, string userName);
        GetTestDTO GetTest(string testName, string userName);
        void PassTest(PassTestDTO passTestDTO, string userName);
        ICollection<SearchTestDTO> FindTestWhichContainText(string partOrFullTestName, string userName = null);
        ICollection<UserSubscribtionsDTO> UsersSubscriptions(string userName);
    }
}
