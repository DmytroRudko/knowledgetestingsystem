﻿"use strict"

$("#addQuestion").on("click", function () {
    $.ajax({
        url: this.href,
        cache: false,
        success: function (html) { $("#question").append(html); }
    });
    return false;
});

$("#question").on("click", ".deleteQuestion", function () {
    $(this).parents("div.question:first").remove();
    return false;
});


//answer
//$("#question").on("click", "#addAnswer", function (event) {
//        $.ajax({
//            url: this.href,
//            cache: false,
//            success: function (html) { $(event.target).siblings("#answer").append(html); }
//        });
//        return false;
//    });

//$("#answer").on("click", ".deleteAnswer", function () {
//    $(this).parents("div.answer:first").remove();
//    return false;
//});


