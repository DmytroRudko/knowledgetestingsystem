﻿using KnowledgeTestingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KnowledgeTestingSystem.DAL.Interfaces
{
    /// <summary>
    /// Generic repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Get collection of T objects by expression
        /// </summary>
        /// <returns>Collection of T objects by expression or null</returns>
        IEnumerable<T> GetAll(Expression<Func<T, bool>> exp);
        /// <summary>
        /// Check if object T exists
        /// </summary>
        /// <returns>True if object exist, otherwise false</returns>
        bool Exists(Expression<Func<T, bool>> predicate);
        /// <summary>
        /// Get single object by expression
        /// </summary>
        /// <returns>Object of type T</returns>
        T Get(Expression<Func<T, bool>> exp);
        /// <summary>
        /// Add object into context
        /// </summary>
        void Add(T item);
        /// <summary>
        /// Update object into context
        /// </summary>
        void Update(T item);
        /// <summary>
        /// Save changes in the context
        /// </summary>
        void Save();  

    }
}
