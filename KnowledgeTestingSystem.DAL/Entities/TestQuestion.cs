﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Entities
{
    public class TestQuestion: BaseEntity
    {
        public string Question { get; set; }
        public Test Test { get; set; }  
        public ICollection<TestAnswer> TestAnswersForQuestion { get; set; }
    }
}
