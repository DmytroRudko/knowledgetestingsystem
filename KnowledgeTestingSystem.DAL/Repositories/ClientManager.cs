﻿using KnowledgeTestingSystem.DAL.EF;
using KnowledgeTestingSystem.DAL.Entities;
using KnowledgeTestingSystem.DAL.Interfaces;
using System.Linq;

namespace KnowledgeTestingSystem.DAL.Repositories
{
    public class ClientManager : IUserProfileManager
    {
        public ApplicationContext Database { get; set; }
        public ClientManager(ApplicationContext db)
        {
            Database = db;
        }

        public UserProfile Get(string userName)
        {
            return Database.UserProfiles.FirstOrDefault(x => x.Name == userName);
        }

        public void Create(UserProfile item)
        {
            Database.UserProfiles.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }

    }
}
