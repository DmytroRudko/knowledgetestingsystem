﻿using KnowledgeTestingSystem.DAL.Entities;
using Microsoft.AspNet.Identity;

namespace KnowledgeTestingSystem.DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> userStore)
                : base(userStore)
        {
        }
    }
}
