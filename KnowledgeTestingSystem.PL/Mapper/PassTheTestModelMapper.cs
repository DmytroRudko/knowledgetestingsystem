﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.PL.Models.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Mapper
{
    public class PassTheTestModelMapper
    {
        public PassTheTestModel MapGetTestDTOToPassTheTestModel (GetTestDTO getTestDTO)
        {
            PassTheTestModel passTheTestModel = new PassTheTestModel();
            passTheTestModel.TestName = getTestDTO.TestName;
            passTheTestModel.TestDurationInMinutes = getTestDTO.TestDurationInMinutes;
            foreach (var item in getTestDTO.Questions)
            {
                passTheTestModel.Questions.Add(new Question
                {
                    QuestionText = item.QuestionText,
                    Answers = GetAnswers(item).ToList()
                });
            }


            return passTheTestModel;
        }

        

        public PassTestDTO MapPassTheTestModelToPassTestDTO(PassTheTestModel passTheTestModel)  
        {
            PassTestDTO passTestDTO = new PassTestDTO();
            passTestDTO.TestName = passTheTestModel.TestName;
            passTestDTO.TestDurationInMinutes = passTheTestModel.TestDurationInMinutes;
            foreach (var item in passTheTestModel.Questions)
            {
                if (item.SelectedAnswer == null)
                {
                    continue;
                }
                passTestDTO.QuestionAnswer.Add(item.QuestionText, item.SelectedAnswer);
            }
            return passTestDTO;
        }

        private ICollection<Answer> GetAnswers(QuestionDTO question) 
        {
            List<Answer> answers = new List<Answer>();
            foreach (var item in question.AnswersDto)
            {
                answers.Add(new Answer { AnswerText = item.AnswerTextDto });
            }
            return answers;
        }
    }
}