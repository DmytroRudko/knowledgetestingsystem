namespace KnowledgeTestingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInTestsSubscribedUserV2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TestsSubscribedUsers", "UserId", "dbo.UserProfiles");
            DropIndex("dbo.TestsSubscribedUsers", new[] { "UserId" });
            DropPrimaryKey("dbo.TestsSubscribedUsers");
            AlterColumn("dbo.TestsSubscribedUsers", "UserId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.TestsSubscribedUsers", new[] { "UserId", "TestId" });
            CreateIndex("dbo.TestsSubscribedUsers", "UserId");
            AddForeignKey("dbo.TestsSubscribedUsers", "UserId", "dbo.UserProfiles", "Id", cascadeDelete: true);
            DropColumn("dbo.TestsSubscribedUsers", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TestsSubscribedUsers", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.TestsSubscribedUsers", "UserId", "dbo.UserProfiles");
            DropIndex("dbo.TestsSubscribedUsers", new[] { "UserId" });
            DropPrimaryKey("dbo.TestsSubscribedUsers");
            AlterColumn("dbo.TestsSubscribedUsers", "UserId", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.TestsSubscribedUsers", "Id");
            CreateIndex("dbo.TestsSubscribedUsers", "UserId");
            AddForeignKey("dbo.TestsSubscribedUsers", "UserId", "dbo.UserProfiles", "Id");
        }
    }
}
