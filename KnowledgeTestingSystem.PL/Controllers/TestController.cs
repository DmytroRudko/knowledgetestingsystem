﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.BL.Infrastructure;
using KnowledgeTestingSystem.BL.Interfaces;
using KnowledgeTestingSystem.PL.Mapper;
using KnowledgeTestingSystem.PL.Models.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KnowledgeTestingSystem.PL.Controllers
{
    public class TestController : Controller
    {
        private CreateTestMapper testMapper;
        private ITestService testService;

        //ctor
        public TestController(ITestService service)
        {
            testService = service;
        }

        // GET: Test
        //public ActionResult Index()
        //{
        //    return View();
        //}

        [Authorize]
        // GET: Test/Create
        public ActionResult Create()
        {
            CreateTestModel model = new CreateTestModel();
            return View(model);
        }

        // POST: Test/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(CreateTestModel model)
        {
            if (ModelState.IsValid)
            {
                testMapper = new CreateTestMapper();
                var testDto = testMapper.MapCreateTestModelToCreateTestDTO(model, User.Identity.Name);
                OperationDetails operationDetails = testService.CreateTest(testDto);
                if (operationDetails.Succedeed)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
            }
            return View(model);

        }

        [Authorize]
        public ViewResult PassTheTest(string testName)
        {
            var test = testService.GetTest(testName,User.Identity.Name);
            if (test != null)
            {
                PassTheTestModelMapper mapper = new PassTheTestModelMapper();
                var passTheTestModel = mapper.MapGetTestDTOToPassTheTestModel(test);
                return View(passTheTestModel);
            }
            //throw new HttpException(404, "Page Not Found");
            return View("~/Views/Home/Index.cshtml");
        }

        [Authorize]
        [HttpPost]
        public ViewResult PassTheTest(PassTheTestModel passTheTestModel)
        {
            PassTheTestModelMapper mapper = new PassTheTestModelMapper();
            testService.PassTest(mapper.MapPassTheTestModelToPassTestDTO(passTheTestModel), User.Identity.Name);
            
            return View("~/Views/Home/Index.cshtml");
        }


        public ViewResult QuestionEditorRow()
        {
            var element = new TestQuestionModel();
            return View("AddQuestionPartial", element);
        }


        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string filter)
        {
            ViewBag.filter = filter;
            return View();
        }


        public ActionResult SearchTest(string filter)
        {
            ICollection<SearchTestDTO> listOfTestNames;
            if (User.Identity.IsAuthenticated)
            {
                listOfTestNames = testService.FindTestWhichContainText(filter, User.Identity.Name);
            }
            else
            {
                listOfTestNames = testService.FindTestWhichContainText(filter);
            }
            SearchTestMapper searchTestMapper = new SearchTestMapper();
            var listOfSearchTestModel = searchTestMapper.MapSearchTestDTOToSearchTestModel(listOfTestNames);
            return View("SearchTestPartial", listOfSearchTestModel);

        }

        [Authorize]
        public ActionResult Subscribe(string testName)
        {
            OperationDetails opDet = testService.SubscribeToTest(testName, User.Identity.Name);
            return Redirect(Request.UrlReferrer.ToString());
        }

        [Authorize]
        public ActionResult Subscriptions()
        {
            UserSubscribedTestsMapper mapper = new UserSubscribedTestsMapper();
            List<UserSubscribedTestsModel> userSubscribedTests = mapper.MapUserSubscribtionsDTOToUserSubscribedTestsModel(testService.UsersSubscriptions(User.Identity.Name)).ToList();
            return View(userSubscribedTests);
        }

    }
}
