﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Models.Test
{
    public class PassTheTestModel
    {

        public PassTheTestModel()
        {
            Questions = new List<Question>();
        }
        public int TestDurationInMinutes { get; set; }  
        public string TestName { get; set; }
        public List<Question> Questions { get; set; }

    }

    public class Question
    {
        public Question()
        {
            Answers = new List<Answer>();
        }
        public string QuestionText { get; set; }
        public List<Answer> Answers { get; set; }

        public string SelectedAnswer { get; set; }
    }

    public class Answer
    {
        public string AnswerText { get; set; }
    }


}