﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.PL.Models.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Mapper
{
    public class UserSubscribedTestsMapper
    {
        public ICollection<UserSubscribedTestsModel> MapUserSubscribtionsDTOToUserSubscribedTestsModel(ICollection<UserSubscribtionsDTO> collection)
        {
            List<UserSubscribedTestsModel> userSubscribedTests = new List<UserSubscribedTestsModel>();
            foreach (var item in collection)
            {
                userSubscribedTests.Add(new UserSubscribedTestsModel
                {
                    TestName = item.TestName,
                    IsFinished = item.IsFinished,
                    IsFinishedSuccessfully = item.IsFinishedSuccessfully,
                    PercentageOfPassing = item.PercentageOfPassing,
                    TimeOfPassing = item.TimeOfPassing
                });
            }
            return userSubscribedTests;
        }
    }
}