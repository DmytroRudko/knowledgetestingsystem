namespace KnowledgeTestingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTestAnswerTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Answer = c.String(),
                        IsTrue = c.Boolean(nullable: false),
                        TestQuestion_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestQuestions", t => t.TestQuestion_Id)
                .Index(t => t.TestQuestion_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestAnswers", "TestQuestion_Id", "dbo.TestQuestions");
            DropIndex("dbo.TestAnswers", new[] { "TestQuestion_Id" });
            DropTable("dbo.TestAnswers");
        }
    }
}
