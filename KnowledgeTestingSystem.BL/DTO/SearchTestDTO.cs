﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.DTO
{
    public class SearchTestDTO
    {
        public SearchTestDTO()
        {
            IsSubscribed = false;
        }
        public string TestName { get; set; }
        public bool IsSubscribed { get; set; }
    }
}
