﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeTestingSystem.DAL.Entities
{
    public class Test : BaseEntity
    {
        [MaxLength(128)]
        public string TestName { get; set; }
        public string TestDesctiption { get; set; }

        [Range(0.0, 100.0)]
        public double PercentageToComplete { get; set; }
        public TimeSpan TestDuration { get; set; }
        
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public string TestOwnerId { get; set; } 
        public virtual UserProfile TestOwner { get; set; }

        public virtual ICollection<TestQuestion> TestQuestions { get; set; }
        public virtual ICollection<TestsSubscribedUser> TestsSubscribedUsers { get; set; }
    }
}
