﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.PL.Models.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Mapper
{
    public class CreateTestMapper
    {
        private CreateTestDTO createTestDTO;

        public CreateTestDTO MapCreateTestModelToCreateTestDTO(CreateTestModel createTestModel, string testCreator)
        {
            createTestDTO = new CreateTestDTO();
            createTestDTO.UserCreator = testCreator;
            createTestDTO.TestName = createTestModel.TestName;
            createTestDTO.TestDesctiption = createTestModel.TestDesctiption;
            createTestDTO.PercentageToComplete = createTestModel.PercentageToComplete;
            createTestDTO.TestMinutesDuration = createTestModel.TestDuration;
            createTestDTO.CreateQuestionDTOs = new List<CreateQuestionDTO>();
            foreach (var item in createTestModel.TestQuestions)
            {
                createTestDTO.CreateQuestionDTOs.Add(MapTestQuestionModelToCreateQuestionDTO(item));
            }

            return createTestDTO;
        }
        private CreateQuestionDTO MapTestQuestionModelToCreateQuestionDTO(TestQuestionModel testQuestionModel)
        {
            CreateQuestionDTO createQuestionDTO = new CreateQuestionDTO();
            createQuestionDTO.Question = testQuestionModel.Question;
            createQuestionDTO.CreateAnswerDTOs = MapTestQuestionModelToCreateAnswerDTO(testQuestionModel);
            return createQuestionDTO;
        }
        private ICollection<CreateAnswerDTO> MapTestQuestionModelToCreateAnswerDTO(TestQuestionModel testQuestionModel)
        {
            List<CreateAnswerDTO> collection = new List<CreateAnswerDTO>();
            collection.Add(new CreateAnswerDTO { Answer = testQuestionModel.AnswerFirst, IsTrue = false });
            collection.Add(new CreateAnswerDTO { Answer = testQuestionModel.AnswerSecond, IsTrue = false });
            collection.Add(new CreateAnswerDTO { Answer = testQuestionModel.AnswerThird, IsTrue = false });
            collection.Add(new CreateAnswerDTO { Answer = testQuestionModel.AnswerFourth, IsTrue = false });
            collection[testQuestionModel.TrueAnswer - 1].IsTrue = true;
            return collection;
        }
    }
}