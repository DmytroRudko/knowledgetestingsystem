﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.BL.Interfaces;
using KnowledgeTestingSystem.DAL.Entities;
using KnowledgeTestingSystem.DAL.Interfaces;
using KnowledgeTestingSystem.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.Mapper
{
    public class TestDTOMapper
    {
        private IUserProfileService userProfileService;
        private IRepository<Test> testRepository;

        public TestDTOMapper(IUserProfileService userProfileService, IRepository<Test> testRepository)
        {
            this.userProfileService = userProfileService;
            this.testRepository = testRepository;
        }
        public Test MapTestDTOToTestEntity(CreateTestDTO createTestDTO)
        {
            Test test = new Test();
            test.TestName = createTestDTO.TestName;
            test.TestDesctiption = createTestDTO.TestDesctiption;
            test.TestDuration = TimeSpan.FromMinutes(createTestDTO.TestMinutesDuration);
            test.IsDeleted = false;
            test.TestQuestions = MapCreateQuestionDTOToTestQuestionCollection(createTestDTO.CreateQuestionDTOs);
            test.TestOwnerId = userProfileService.GetUser(createTestDTO.UserCreator).Id;
            test.PercentageToComplete = createTestDTO.PercentageToComplete;
            return test;
        }

        public SearchTestDTO MapTestToSearchTestDTO(Test test)
        {
            return new SearchTestDTO() { TestName = test.TestName };
        }

        public GetTestDTO MapTestToGetTestDTO(Test test)
        {
            GetTestDTO testDTO = new GetTestDTO();
            testDTO.TestName = test.TestName;
            testDTO.TestDurationInMinutes = (test.TestDuration.Hours * 60) + test.TestDuration.Minutes;
            foreach (var item in test.TestQuestions)
            {
                testDTO.Questions.Add(new QuestionDTO
                {
                    QuestionText = item.Question,
                    AnswersDto = GetAnswers(item).ToList()
                });
            }
            return testDTO;
        }

        private ICollection<AnswerDTO> GetAnswers(TestQuestion testQuestions)
        {
            List<AnswerDTO> answers = new List<AnswerDTO>();
            foreach (var item in testQuestions.TestAnswersForQuestion)
            {
                answers.Add(new AnswerDTO { AnswerTextDto = item.Answer });
            }
            return answers;
        }

        private ICollection<TestQuestion> MapCreateQuestionDTOToTestQuestionCollection(ICollection<CreateQuestionDTO> createQuestionDTOs)
        {
            List<TestQuestion> testQuestions = new List<TestQuestion>();
            foreach (var item in createQuestionDTOs)
            {
                testQuestions.Add(new TestQuestion
                {
                    Question = item.Question,
                    TestAnswersForQuestion = MapCreateTestAnswerDTOToTestAnswerCollection(item.CreateAnswerDTOs)
                });
            }
            return testQuestions;
        }

        public ICollection<UserSubscribtionsDTO> MapTestsSubscribedUserToUserSubscribtionsDTOCollection(ICollection<TestsSubscribedUser> testsSubscribedUsers)
        {
            List<UserSubscribtionsDTO> userSubscribtionsDTOs = new List<UserSubscribtionsDTO>();
            foreach (var item in testsSubscribedUsers)
            {
                var test = testRepository.Get(p => p.Id == item.TestId);
                userSubscribtionsDTOs.Add(new UserSubscribtionsDTO
                {
                    TestName = test.TestName,
                    IsFinished = item.IsFinished,
                    IsFinishedSuccessfully = item.PercentageOfPassing >= test.PercentageToComplete,
                    PercentageOfPassing = (int)Math.Round(item.PercentageOfPassing, MidpointRounding.AwayFromZero),
                    TimeOfPassing = $"{item.TimeOfPassingTheTest.Minutes}:{item.TimeOfPassingTheTest.Seconds}"
                });
            }
            return userSubscribtionsDTOs;
        }

        private ICollection<TestAnswer> MapCreateTestAnswerDTOToTestAnswerCollection(ICollection<CreateAnswerDTO> createAnswerDTOs)
        {
            List<TestAnswer> testAnswers = new List<TestAnswer>();
            foreach (var item in createAnswerDTOs)
            {
                testAnswers.Add(new TestAnswer { Answer = item.Answer, IsTrue = item.IsTrue });
            }
            return testAnswers;
        }
    }
}
