﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.PL.Models.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Mapper
{
    public class SearchTestMapper
    {
        public ICollection<SearchTestModel> MapSearchTestDTOToSearchTestModel(ICollection<SearchTestDTO> searchTests)
        {
            List<SearchTestModel> searchTestModels = new List<SearchTestModel>();
            foreach (var item in searchTests)
            {
                searchTestModels.Add(new SearchTestModel { TestName = item.TestName, IsSubscribed = item.IsSubscribed});
            }
            return searchTestModels;
        }


    }
}