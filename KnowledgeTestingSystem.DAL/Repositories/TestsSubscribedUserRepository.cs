﻿using KnowledgeTestingSystem.DAL.EF;
using KnowledgeTestingSystem.DAL.Entities;
using KnowledgeTestingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Repositories
{
    public class TestsSubscribedUserRepository : ITestsSubscribedUserRepository
    {
        private ApplicationContext context;
        DbSet<TestsSubscribedUser> dbSet;

        public TestsSubscribedUserRepository(DbContext dbContext)
        {
            this.context = (ApplicationContext)dbContext;
            dbSet = context.TestsSubscribedUsers;
        }

        public void Add(Test test, UserProfile userProfile)
        {
            dbSet.Add(new TestsSubscribedUser()
            {
                TestId = test.Id,
                UserId = userProfile.Id,
                TimeOfPassingTheTest = TimeSpan.FromMinutes(0),
                IsFinished = false,
                PercentageOfPassing = 0
            });
            context.SaveChanges();
        }

        public Test GetTest(string testName)
        {
            return context.Tests.Where(x => x.TestName == testName).Include("TestQuestions.TestAnswersForQuestion").FirstOrDefault();
        }

        public ICollection<TestsSubscribedUser> GetTestsSubscribedUser(UserProfile userProfile)
        {
            return dbSet.Where(x => x.UserId == userProfile.Id).ToList();
        }

        public TestsSubscribedUser GetTestsSubscribedUser(Test test, UserProfile userProfile)
        {
            return dbSet.First(x => x.TestId == test.Id && x.UserId == userProfile.Id);
        }

        public ICollection<Test> GetUserSubscribedTest(UserProfile userProfile)
        {
            return dbSet.Where(x => x.UserId == userProfile.Id).Select(x => x.Test).ToList();
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
