﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.DTO
{
    public class UserSubscribtionsDTO
    {
        public string TestName { get; set; }
        public bool IsFinished { get; set; }
        public int PercentageOfPassing { get; set; }
        public string TimeOfPassing { get; set; }
        public bool IsFinishedSuccessfully { get; set; }
    }
}
