﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Models.Test
{
    public class TestQuestionModel
    {
        [MinLength(3, ErrorMessage = "Minimal lenght of question 3 symbols")]
        [Required(AllowEmptyStrings = false, ErrorMessage ="Enter the question")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Question { get; set; }

        [MinLength(1, ErrorMessage = "Minimal lenght of answer 3 symbols")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter the question")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AnswerFirst  { get; set; }
        [MinLength(1, ErrorMessage = "Minimal lenght of answer 3 symbols")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter the question")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AnswerSecond { get; set; }
        [MinLength(1, ErrorMessage = "Minimal lenght of answer 3 symbols")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter the question")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AnswerThird { get; set; }
        [MinLength(1, ErrorMessage = "Minimal lenght of answer 3 symbols")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter the question")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AnswerFourth { get; set; } 
        [Range(1,4)]
        public int TrueAnswer { get; set; }
    }
}