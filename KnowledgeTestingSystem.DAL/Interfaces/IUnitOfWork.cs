﻿using KnowledgeTestingSystem.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        IUserProfileManager UserProfileManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
    }
}
