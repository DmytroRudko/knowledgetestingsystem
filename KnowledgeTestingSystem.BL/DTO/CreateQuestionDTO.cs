﻿using System.Collections.Generic;

namespace KnowledgeTestingSystem.BL.DTO
{
    public class CreateQuestionDTO
    {
        public string Question { get; set; }

        public ICollection<CreateAnswerDTO> CreateAnswerDTOs { get; set; } 
    }
}
