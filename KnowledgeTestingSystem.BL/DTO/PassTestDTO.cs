﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.DTO
{
    public class PassTestDTO
    {
        public PassTestDTO()
        {
            QuestionAnswer = new Dictionary<string, string>();
        }
        public int TestDurationInMinutes { get; set; }
        public string TestName { get; set; }
        public Dictionary<string,string> QuestionAnswer { get; set; }
    }
}
