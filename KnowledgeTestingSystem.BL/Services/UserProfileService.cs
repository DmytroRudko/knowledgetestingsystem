﻿using KnowledgeTestingSystem.BL.Interfaces;
using KnowledgeTestingSystem.DAL.Entities;
using KnowledgeTestingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.Services
{
    public class UserProfileService : IUserProfileService
    {
        private IUserProfileRepository userRepository;

        public UserProfileService(IUserProfileRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public UserProfile GetUser(string userName)
        {
            return userRepository.GetUser(userName);
        }
    }
}
