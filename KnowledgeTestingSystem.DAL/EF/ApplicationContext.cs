﻿using KnowledgeTestingSystem.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace KnowledgeTestingSystem.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() { }
        public ApplicationContext(string conectionString) : base(conectionString) { }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestsSubscribedUser> TestsSubscribedUsers { get; set; }
        public DbSet<TestQuestion> TestQuestions { get; set; }

    }
}
