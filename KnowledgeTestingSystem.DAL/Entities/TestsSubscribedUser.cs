﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Entities
{
    public class TestsSubscribedUser 
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        [Key, Column(Order = 1)]
        public int? TestId { get; set; } 
        public virtual UserProfile User{ get; set; }
        public virtual Test Test{ get; set; }

        [DefaultValue(false)]
        public bool IsFinished { get; set; }

        [Range(0.0, 100.0)]
        public double PercentageOfPassing { get; set; }
        public TimeSpan TimeOfPassingTheTest { get; set; }  
    }
}
