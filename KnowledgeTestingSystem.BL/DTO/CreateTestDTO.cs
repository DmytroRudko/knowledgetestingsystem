﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.DTO
{
    public class CreateTestDTO
    {
        public string UserCreator { get; set; }
        public string TestName { get; set; }

        public string TestDesctiption { get; set; }

        public double PercentageToComplete { get; set; }
        public int TestMinutesDuration { get; set; }    
        public List<CreateQuestionDTO> CreateQuestionDTOs { get; set; }
    }
}
