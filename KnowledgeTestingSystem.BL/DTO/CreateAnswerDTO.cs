﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.DTO
{
    public class CreateAnswerDTO
    {
        public string Answer { get; set; }
        public bool IsTrue { get; set; }    
    }
}
