namespace KnowledgeTestingSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTestSubsctiberUserTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestsSubscribedUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsFinished = c.Boolean(nullable: false),
                        PercentageOfPassing = c.Double(nullable: false),
                        TimeOfPassingTheTest = c.Time(nullable: false, precision: 7),
                        Test_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tests", t => t.Test_Id)
                .ForeignKey("dbo.UserProfiles", t => t.User_Id)
                .Index(t => t.Test_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestsSubscribedUsers", "User_Id", "dbo.UserProfiles");
            DropForeignKey("dbo.TestsSubscribedUsers", "Test_Id", "dbo.Tests");
            DropIndex("dbo.TestsSubscribedUsers", new[] { "User_Id" });
            DropIndex("dbo.TestsSubscribedUsers", new[] { "Test_Id" });
            DropTable("dbo.TestsSubscribedUsers");
        }
    }
}
