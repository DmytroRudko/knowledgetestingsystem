﻿using KnowledgeTestingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.Interfaces
{
    public interface IUserProfileService
    {
        UserProfile GetUser(string userName);
    }
}
