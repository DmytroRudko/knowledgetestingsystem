﻿using KnowledgeTestingSystem.DAL.Entities;
using KnowledgeTestingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Repositories
{
    public class EFGenericRepository<T> : IRepository<T> where T : BaseEntity
    {
        private DbContext dbContext;
        private DbSet<T> dbSet;

        public EFGenericRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
            this.dbSet = dbContext.Set<T>();
        }

        public void Add(T item)
        {
            dbSet.Add(item);
            Save();
        }

        public bool Exists(Expression<Func<T, bool>> predicate)
        {
            return dbSet.Any(predicate);
        }

        public T Get(Expression<Func<T, bool>> exp)
        {
            return dbSet.FirstOrDefault(exp);
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> exp)
        {
            return dbSet.Where(exp);
        }

        public void Save()
        {
            dbContext.SaveChanges();
        }

        //todo: implement update method
        public void Update(T item)
        {
            throw new NotImplementedException();
        }
    }
}
