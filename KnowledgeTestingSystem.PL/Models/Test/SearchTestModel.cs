﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Models.Test
{
    public class SearchTestModel
    {
        public string TestName { get; set; }

        public bool IsSubscribed { get; set; }
    }
}