﻿using KnowledgeTestingSystem.DAL.EF;
using KnowledgeTestingSystem.DAL.Interfaces;
using KnowledgeTestingSystem.DAL.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.BL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<DbContext>().To<ApplicationContext>().WithConstructorArgument(connectionString);
            Bind(typeof(IRepository<>)).To(typeof(EFGenericRepository<>));
            Bind<IUserProfileRepository>().To<UserProfileRepository>();
            Bind<ITestsSubscribedUserRepository>().To<TestsSubscribedUserRepository>();
        }
    }
}
