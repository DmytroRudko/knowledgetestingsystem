﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KnowledgeTestingSystem.DAL.Entities
{
    public class UserProfile
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public ICollection<Test> TestsCreaterByCurrentUser { get; set; }
        public ICollection<TestsSubscribedUser> TestsSubscribedUser { get; set; }
    }
}
