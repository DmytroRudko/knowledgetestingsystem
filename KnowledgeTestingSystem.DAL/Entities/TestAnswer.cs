﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Entities
{
    public class TestAnswer : BaseEntity
    {
        public string Answer { get; set; }
        public bool IsTrue { get; set; }
        public TestQuestion TestQuestion { get; set; }
    }
}
