﻿using KnowledgeTestingSystem.BL.DTO;
using KnowledgeTestingSystem.BL.Infrastructure;
using KnowledgeTestingSystem.BL.Interfaces;
using KnowledgeTestingSystem.BL.Mapper;
using KnowledgeTestingSystem.DAL.Entities;
using KnowledgeTestingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KnowledgeTestingSystem.BL.Services
{
    public class TestService : ITestService
    {
        private readonly IRepository<Test> testRepository;
        private readonly IUserProfileService userProfileService;
        private readonly ITestsSubscribedUserRepository testsSubscribedUserRepository;
        public TestService(IRepository<Test> repository, IUserProfileService userProfileService, ITestsSubscribedUserRepository testsSubscribedUserRepository)
        {
            this.testRepository = repository;
            this.userProfileService = userProfileService;
            this.testsSubscribedUserRepository = testsSubscribedUserRepository;
        }
        public OperationDetails CreateTest(CreateTestDTO createTestDTO)
        {

            TestDTOMapper mapper = new TestDTOMapper(userProfileService, testRepository);
            var testEntity = mapper.MapTestDTOToTestEntity(createTestDTO);
            if (testRepository.Exists(x => x.TestName == testEntity.TestName))
            {
                return new OperationDetails(false, "Test with this name already exists ", "");
            }
            testRepository.Add(testEntity);
            return new OperationDetails(true, "Test were created successfully", "");
        }

        public ICollection<SearchTestDTO> FindTestWhichContainText(string partOrFullTestName, string userName = null)
        {

            var foundTests = testRepository.GetAll(x => x.TestName.Contains(partOrFullTestName) && !x.IsDeleted);
            TestDTOMapper testDTOMapper = new TestDTOMapper(userProfileService,testRepository);

            List<SearchTestDTO> tests = new List<SearchTestDTO>();
            if (foundTests != null)
            {
                foreach (var item in foundTests)
                {
                    tests.Add(testDTOMapper.MapTestToSearchTestDTO(item));
                }
                if (userName != null)
                {
                    UserProfile user = userProfileService.GetUser(userName);
                    var userTests = testsSubscribedUserRepository.GetUserSubscribedTest(user);
                    if (userTests == null)
                    {
                        return tests;
                    }
                    List<SearchTestDTO> userSubscribedTests = new List<SearchTestDTO>();
                    foreach (var item in userTests)
                    {
                        userSubscribedTests.Add(testDTOMapper.MapTestToSearchTestDTO(item));
                    }
                    FindTestsTheUserIsSubscribedTo(tests,userSubscribedTests);

                    return tests;
                }
            }

            return tests;
        }

        public GetTestDTO GetTest(string testName, string userName)
        {
            if (testRepository.Exists(x=>x.TestName == testName))
            {
                //TODO: check if user already pass the test
                TestDTOMapper mapper = new TestDTOMapper(userProfileService, testRepository);
                return mapper.MapTestToGetTestDTO(testsSubscribedUserRepository.GetTest(testName));
            }
            return null;
        }

        public void PassTest(PassTestDTO passTestDTO,string userName)
        {
            var test = testsSubscribedUserRepository.GetTest(passTestDTO.TestName);
            int counter = 0;
            foreach (var item in passTestDTO.QuestionAnswer)
            {
                if ( test.TestQuestions.First(tq => tq.Question == item.Key)
                    .TestAnswersForQuestion.First(tafq => tafq.Answer == item.Value)
                    .IsTrue)
                {
                    counter++;
                }
            }
            var userSubscribedTest = testsSubscribedUserRepository.GetTestsSubscribedUser(test, userProfileService.GetUser(userName));
            userSubscribedTest.IsFinished = true;
            if (counter == 0)
            {
                userSubscribedTest.PercentageOfPassing = 0;
            }
            else
            {
                userSubscribedTest.PercentageOfPassing = Math.Round((double)counter / (double)test.TestQuestions.Count() * 100);
            }
            userSubscribedTest.TimeOfPassingTheTest = TimeSpan.FromMinutes(0);
            testsSubscribedUserRepository.Save();
        }

        public OperationDetails SubscribeToTest(string testName, string userName)
        {
            if (!testRepository.Exists(x => x.TestName == testName))
            {
                return new OperationDetails(false, "Test doesn't exists", "testName");
            }
            var user = userProfileService.GetUser(userName);
            if (testsSubscribedUserRepository.GetUserSubscribedTest(user).Any(x => x.TestName == testName))
            {
                return new OperationDetails(false, "You're alreadySubscribedToTest", "testName");
            }
            testsSubscribedUserRepository.Add(testRepository.Get(x => x.TestName == testName), user);
            return new OperationDetails(true, "You have succesfully subscribed", "");
        }

        public ICollection<UserSubscribtionsDTO> UsersSubscriptions(string userName)
        {
            UserProfile userProfile = userProfileService.GetUser(userName);
            var usersTestSubscribers = testsSubscribedUserRepository.GetTestsSubscribedUser(userProfile);
            TestDTOMapper testDTOMapper = new TestDTOMapper(userProfileService, testRepository);
            return testDTOMapper.MapTestsSubscribedUserToUserSubscribtionsDTOCollection(usersTestSubscribers);
        }

        private void FindTestsTheUserIsSubscribedTo(ICollection<SearchTestDTO> foundTests, ICollection<SearchTestDTO> userSubscribedTests)
        {
            foreach (var item in foundTests.Where(ft => userSubscribedTests.Any(ust => ust.TestName == ft.TestName)))
            {
                item.IsSubscribed = true;
            }
        }
    }
}
