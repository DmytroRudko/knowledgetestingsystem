﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeTestingSystem.PL.Models.Test
{
    public class UserSubscribedTestsModel
    {
        public string TestName { get; set; }
        public bool IsFinished { get; set; }
        public int PercentageOfPassing { get; set; }
        public string TimeOfPassing { get; set; }
        public bool IsFinishedSuccessfully { get; set; }    
    }
}