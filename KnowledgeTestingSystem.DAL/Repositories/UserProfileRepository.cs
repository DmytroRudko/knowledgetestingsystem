﻿using KnowledgeTestingSystem.DAL.EF;
using KnowledgeTestingSystem.DAL.Entities;
using KnowledgeTestingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.DAL.Repositories
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private ApplicationContext context;
        DbSet<UserProfile> dbSet;
        public UserProfileRepository(DbContext dbContext)
        {
            this.context = (ApplicationContext)dbContext;
            dbSet = context.UserProfiles;
        }
        public UserProfile GetUser(string userName)
        {
            return dbSet.AsNoTracking().FirstOrDefault(p => p.Name == userName);
        }
    }
}
